import React, { Component } from "react";
import Item from "./item";
import uuid from 'uuid';

class Items extends Component {
  render() {
    return (
      <div>
        {this.props.items.map(item => (
          <Item
            key={uuid.v4()}
            onDelete={this.props.onItemDelete}
            onChecked={this.props.onItemChecked}
            item={item} // obsahuje i své unikátní id
          />
        ))}
      </div>
    );
  }
}
export default Items;
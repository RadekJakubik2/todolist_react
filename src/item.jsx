import React, { Component } from "react";

class Item extends Component {
  render() {
    const { item } = this.props;
    const text = item.checked ? <del>{item.value}</del> : item.value;
    return (
      <div>
        <input type="checkbox" onClick={() => this.props.onChecked(item.id, !item.checked)} defaultChecked={item.checked} />
        <label>{text}</label>
        <button onClick={() => this.props.onDelete(item.id)}>
          Delete
        </button>
      </div>
    );
  }
}
export default Item;
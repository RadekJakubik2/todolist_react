import React, { Component } from 'react';
import Items from './items';

class App extends Component {
  state = {
    items: [],
    filteredItems: [],
    textboxValue: ''
  };

  onFormSubmit = (evt) => {
    let idsArray = this.state.items.map(i => i.id);
    var newId = Math.max(...idsArray) + 1;

    const item = { id: newId, value: this.state.textboxValue, checked: false }; // tohle asi líp. Vkládat nějak asi objekt. Do úvodního pole taky new Item(,,);
    const items = [...this.state.items, item];
    this.setState({ items: items, filteredItems: items, textboxValue: '' });
    evt.preventDefault();
  };

  onTextboxChange = (evt) => {
    let filteredItems = this.filterItemsContainString(this.state.items, evt.target.value);
    this.setState({ filteredItems: filteredItems, textboxValue: evt.target.value });
  };

  onTextboxWithoutChange = () => {
    let filteredItems = this.filterItemsContainString(this.state.items, this.state.textboxValue);
    this.setState({ filteredItems: filteredItems });
  };

  onItemDelete = itemId => {
    const items = this.state.items.filter(i => i.id !== itemId);
    this.setState({ items: items }, () => this.onTextboxWithoutChange());
  }

  onItemChecked = (itemId, checked) => {
    const items = [...this.state.items];
    items.find(i => i.id === itemId).checked = checked;
    this.setState({ items: items });
  }

  filterItemsContainString(array, string) {
    return array.filter(i => i.value.includes(string));
  }

  componentWillMount() {
    let storageItems = this.getTweetsFromStorage();
    this.setState({ filteredItems: storageItems, items: storageItems });
  }

  componentDidUpdate() {
    localStorage.setItem('items', JSON.stringify(this.state.items));
  }

  getTweetsFromStorage() {
    let items;
    const itemsLS = localStorage.getItem('items');
    if (itemsLS === null) {
      items = [
        { id: 1, value: 'aaa', checked: true },
        { id: 2, value: 'bbb', checked: false },
        { id: 3, value: 'ccc', checked: false },
        { id: 4, value: 'ddd', checked: false }
      ];
    } else {
      items = JSON.parse(itemsLS);
    }
    return items;
  }

  render() {
    return (
      <div>
        <h1>5) Sign Up Sheet</h1>
        <form onSubmit={this.onFormSubmit}>
          <input
            onChange={this.onTextboxChange}
            placeholder='Item to do...'
            value={this.state.textboxValue}
          />

          <input type='submit' value='Odeslat' />
        </form>

        <div>
          <h3>Names</h3>
          <Items
            items={this.state.filteredItems}
            onItemDelete={this.onItemDelete}
            onItemChecked={this.onItemChecked}
          />
        </div>
      </div>
    );
  }
}

export default App;
